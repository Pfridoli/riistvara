#include <string.h>
#include <time.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"

#define BLINK_DELAY_MS 100
#define UART_BAUD 9600
#define UART_STATUS_MASK 0x00FF

typedef enum {
    door_opening,
    door_open,
    door_closing,
    door_closed
} door_state_t;

door_state_t door_state = door_closed;

volatile uint32_t system_time;
static microrl_t rl;
static microrl_t *prl = &rl;

/*Init RFID-RC522*/
static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}

/*Init microcontroller*/
static inline void init_micro(void)
{
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);
}

/* Init ports for leds*/
static inline void init_leds(void)
{
    // Sets ports for output
    DDRA |= _BV(DDA0);
    DDRA |= _BV(DDA2);
    DDRA |= _BV(DDA4);
    DDRB |= _BV(DDB7);
    // Turns off yellow led
    PORTB &= ~_BV(PORTB7);
}

/*Init system timer*/
static inline void init_sys_timer(void)
{
    // Set counter to random number 0x19D5F539 in HEX. Set it to 0 if you want
    // counter_1 = 0;
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}

/*Init uart, sys_timer, lcd_screen*/
static inline void init_hw(void)
{
    DDRD |= _BV(DDD3);
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    init_sys_timer();
    sei();
    lcd_init();
    lcd_clrscr();
}

/*Writes out name to lcd screen*/
static inline void show_lcd(void)
{
    lcd_puts_P(PSTR(LCD_NAME));
    lcd_goto(LCD_ROW_2_START);
    lcd_puts_P(PSTR("Door is Closed"));
}

/*Starts heartbeating*/
static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(PORTA2);
        prev_time = now;
    }
}

/* Init error console as stderr in UART1 and print user code info */
static inline void show_errcon(void)
{
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
}

/* Gives current time (for rfid scanner) */
static inline uint32_t current_time(void)
{
    uint32_t cur_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        cur_time = system_time;
    }
    return cur_time;
}

/* Trying to impersonate door status for rfid scanner */
static inline void door_status()
{
    switch (door_state) {
    case door_opening:
        DDRA ^= _BV(DDA2);
        PORTA |= _BV(PORTA0);
        door_state = door_open;
        break;

    case door_open:
        break;

    case door_closing:
        door_state = door_closed;
        PORTA &= ~_BV(PORTA0);
        DDRA ^= _BV(DDA2);
        break;

    case door_closed:
        break;
    }
}

/* Card scanner for opening the door */
static inline void rfid_scanner(void)
{
    Uid uid;
    Uid *uid_ptr = &uid;
    bool status;
    bool used_error;
    static bool message_status;
    static bool door_open_status;
    uint32_t time_cur = current_time();
    static uint32_t message_start;
    static uint32_t door_open_start;
    static uint32_t read_start;
    static char *used_name;
    char *uidName;
    byte bufferATQA[10];
    byte bufferSize[10];

    if ((read_start + 1) < time_cur) {
        if (PICC_IsNewCardPresent()) {
            read_start = time_cur;
            PICC_ReadCardSerial(&uid);
            uidName = bin2hex(uid_ptr->uidByte, uid_ptr->size);
            card_t *checker = head;

            while (checker != NULL) {
                if (strcmp(checker->UID, uidName) == 0) {
                    status = true;
                    break;
                }

                status = false;
                checker = checker->next;
            }

            if (status) {
                if (checker->name != used_name || used_name == NULL) {
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts(checker->name);
                    used_name = checker->name;
                    used_error = false;
                }

                if (door_state != door_open) {
                    door_state = door_opening;
                    door_open_status = true;
                }

                door_open_start = time_cur;
            } else {
                if (!used_error) {
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts_P(PSTR("Unknown user"));
                    used_error = true;
                    used_name = NULL;
                }

                if (door_state != door_closed) {
                    door_state = door_closing;
                    door_open_status = false;
                }
            }

            free(uidName);
            message_status = true;
            message_start = time_cur;
            PICC_WakeupA(bufferATQA, bufferSize);
        }
    }

    if ((message_start + 5) < time_cur && message_status) {
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);
        lcd_puts_P(PSTR("Door is Closed"));
        used_error = false;
        used_name = NULL;
        message_status = false;
    }

    if ((door_open_start + 3) < time_cur && door_open_status) {
        door_state = door_closing;
        door_open_status = false;
    }

    door_status(door_state);
}


/*Main program*/
void main(void)
{
    init_leds();
    init_hw();
    show_errcon();
    uart0_puts_p(PSTR(ST_NAME));
    show_lcd();
    init_micro();
    init_rfid_reader();

    while (1) {
        heartbeat();
        rfid_scanner();
        //CLI commands are handled in cli_execute()
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}

/* Counter 1 ISR */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
    system_time++;
}
