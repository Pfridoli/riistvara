#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define VER_FW "Version: "FW_VERSION" built on: "__DATE__" "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: "__AVR_LIBC_VERSION_STRING__" avr-gcc version: "__VERSION__"\r\n"
#define ST_NAME "Peeter Fridolin \r\n"
#define LCD_NAME "Peeter Fridolin"
#define C_NUMBER "You entered number "
#define W1_NUMBER "Argument is not a decimal number!\r\n"
#define W2_NUMBER "Please enter number between 0 and 9!\r\n"
#define S1_NUMBER "Enter numbers!"
#define S2_NUMBER "Only 0-9 number!"

extern PGM_P const wnumbers[];

#endif
