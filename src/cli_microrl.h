#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H

typedef struct card {
    char *UID;
    char *size;
    char *name;
    struct card *next;
} card_t;

int cli_execute(int argc, const char *const *argv);
extern card_t *head;

#endif
