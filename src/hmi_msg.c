#include <avr/pgmspace.h>

const char m1[] PROGMEM = "zero";
const char m2[] PROGMEM = "one";
const char m3[] PROGMEM = "two";
const char m4[] PROGMEM = "three";
const char m5[] PROGMEM = "four";
const char m6[] PROGMEM = "five";
const char m7[] PROGMEM = "six";
const char m8[] PROGMEM = "seven";
const char m9[] PROGMEM = "eight";
const char m10[] PROGMEM = "nine";

PGM_P const wnumbers[] PROGMEM = {m1, m2, m3, m4, m5, m6, m7, m8, m9, m10};
